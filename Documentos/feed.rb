require 'uri'
require 'open-uri' #http request handler
require 'open_uri_redirections' #Fixes HTTP->HTTPS redirections
require 'nokogiri' #xml parser

class Feed < ActiveRecord::Base

  #Relations
  belongs_to :provider
  belongs_to :category
  has_many :items
  has_many :feed_user_categories
  has_many :user_categories, :through => :feed_user_categories

  #Validations
  #  presence
  validates :url, :presence => true
  #  uniqueness
  validates :url, :uniqueness => true
  
  #Initializations
  before_save do |f|
    #IRB
    #hola = "#{uri.scheme|'http://'}#{uri.host}#{uri.path}"
    #=> "truegoogle.cl"

    begin
      feedUrl = URI.encode(self.url)
      root = Nokogiri::XML(open(feedUrl))
      root.remove_namespaces!
      
      root.xpath('feed').map do |feed|
        f.name = feed.xpath('title').text
      end
      force_updateFeeds
    rescue Exception => e
      #flash.now[:error]="There was an error while checking the feed (#{e.message})"
      puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%" + e.message
      return false
    end
    return true
  end
  after_save :force_updateFeeds


  def updateFeeds
    force_updateFeeds if self.updated_at < (2.hours + 30.minutes).from_now
  end
  
  def force_updateFeeds
    feedUrl = URI.encode(self.url)
    root = Nokogiri::XML(open(feedUrl))
    root.remove_namespaces!
    
    root.xpath('feed/entry').map do |entry|
      title       = parseFormmatedNode entry, 'title'
      abstract    = parseFormmatedNode entry, 'abstract'
      summary     = parseFormmatedNode entry, 'summary'
      description = parseFormmatedNode entry, 'description'
      content     = parseFormmatedNode entry, 'content'
      
      url         = parseAttr     entry, 'link', 'href'
      itemId      = parseTextNode entry, 'id'
      
      if url=~URI::regexp
        link = url
      else
        link = itemId
      end
      link  = URI.join(self.url,url).to_s
      
      
      if !abstract || abstract.length<1
        abstract = summary
      end
      if !abstract || abstract.length<1
        abstract = 'Missing abstract'
      end
      
      if !description || description.length<1
        description = content
      end
      if !description || description.length<1
        description = 'Missing description'
      end
      
      
      
      Item.create(
        :feed_id     => self.id,
        :title       => title,
        :link        => link,
        :abstract    => abstract,
        :description => description
      )
      
      puts "---------------------------"
      puts "title      : #{title}"
      puts "url        : #{url}"
      puts "itemId     : #{itemId}"
      puts "link       : #{link}"
      puts "abstract   : #{abstract}"
      puts "description: #{description}"
      puts "summary    : #{summary}"
      puts "content    : #{content}"
      puts "---------------------------"
      puts ""
      puts ""
    end

    self.updated_at = DateTime.now
    root.xpath('feed').map do |feed|
      self.name = feed.xpath('title').text
    end
  end





  
  ### parse helpers
  def tryGetText(node, xp)
    begin
      text=node.xpath(xp).text
    rescue Exception
      text=nil
    end
  end
  
  def parseFormmatedNode(node, xp)
    text = ''
    begin
      n = node.xpath(xp)
      text = n.text
      type = n.attr('type').text.downcase
      text = h text unless (type=='html' || type=='xhtml')
    rescue Exception
    end
    text
  end
  
  def parseAttr(node, xp, a)
    attr = ''
    begin
      n = node.xpath(xp)
      attr = n.attr(a).text
    rescue Exception => e
    end
    attr
  end
  
  def parseTextNode(node,xp)
    t = ''
    begin
      n = node.xpath(xp)
      t = n.text
    rescue Exception => e
    end
    t
  end
  
end
