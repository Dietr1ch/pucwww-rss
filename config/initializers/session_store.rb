# Be sure to restart your server when you modify this file.

RssReader::Application.config.session_store :encrypted_cookie_store, key: '_rss-reader_session'
