class FeedUserCategory < ActiveRecord::Base

  belongs_to :feed
  belongs_to :user_category

  validates_uniqueness_of :feed, :scope => [:user_category]

end
