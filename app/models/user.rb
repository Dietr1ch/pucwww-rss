require 'gravtastic'
require 'full-name-splitter'

class User < ActiveRecord::Base

  include FullNameSplitter
  include Gravtastic
  gravtastic :secure => true,
             :size   => 80


  
  attr_accessor :password
  before_save :encrypt_password, :downcase_email
  
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
  validates_uniqueness_of :email
  validate :at_least_one_name
  def at_least_one_name
    if ("#{self.first_name}#{self.last_name}".length == 0)
      errors[:base] << ("Please choose at least one name.")
    end
  end

  has_many :user_categories
  has_many :user_read_items
  has_many :items, :through => :user_read_items

  def self.authenticate(email, password)
    user = find_by_email(email.downcase)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.full_name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.password = BCrypt::Engine.generate_salt
      user.email    = "uid-#{user.uid}@#{user.provider}.com"

      #Try to get extended info from Facebook
      unless auth.info.nil?
        user.first_name = auth.info.first_name
        user.last_name  = auth.info.last_name
        user.avatar_url = auth.info.image
        user.email      = auth.info.email
      end
      user.save!
    end
  end
  
  def downcase_email
    self.email = self.email.downcase
    self.role = :role_admin
  end

  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end


  def is_admin?
    self.role == :role_admin
  end

  def avatar
    return self.avatar_url unless self.avatar_url.nil?
    return self.gravatar_url
  end
end
