require 'rinku'

class Item < ActiveRecord::Base
  
  #Order
  #default_scope :order => 'created_at DESC'
  default_scope :order => 'id DESC' #faster and almost equivalent
  
  #Validations
  #  presence
  validates :title, :link, :presence => true
  #  uniqueness
  validates :title, :uniqueness => true
  validates :link, :uniqueness => true

  #Relations
  has_many :users
  has_many :user_read_items
  has_many :users, :through => :user_read_items
  belongs_to :feed
  
  #Integrity Checks
  # before_save :new_entry_check
  #
  before_save do |i|
    begin
      #sanitize
      i.title       = ActionController::Base.helpers.sanitize i.title
      i.description = '' if !i.description
      i.description = ActionController::Base.helpers.sanitize i.description

      #linkify
      i.description = Rinku.auto_link(i.description, :all, 'target="_blank"')
    rescue Exception
      #TODO: Display error on this Item creation to user UTF format error!!
      return false
    end
    return true
  end

  def self.getItems(userID, userCatID, feedID, read, later)

    puts "%%%%%%%%%%%%%%% getItems(uID:#{userID}, uCat:#{userCatID}, fID:#{feedID}, read:#{read}, later:#{later})"

    sqlStr ="
      SELECT distinct
        i.id
        ,i.title
        ,i.link
        ,i.abstract
        ,i.description
        ,i.created_at
        ,i.updated_at
        ,i.image_url
        ,i.feed_id
      FROM
        items i"

    sqlStr << "
        JOIN feeds f                  ON f.id        = i.feed_id
        JOIN feed_user_categories fuc ON fuc.feed_id = f.id
        JOIN user_categories uc       ON uc.id       = fuc.user_category_id
        JOIN users u                  ON u.id        = uc.user_id
      WHERE
        u.id = #{userID}"

    sqlStr << " AND uc.id      = #{userCatID}" if userCatID
    sqlStr << " AND i.feed_id  = #{feedID}"    if feedID

    unless read.nil?
      readCond = ""
      readCond = "NOT" if read=="0"

      puts "read:#{read}"

      sqlStr << " AND i.id #{readCond} IN
                    (
                      SELECT item_id
                      FROM user_read_items
                      WHERE user_id = #{userID}
                    )
      "
    end

    unless later.nil?
      readCond = ""
      readCond = "NOT" if later=="0"

      puts "later:#{later}"

      sqlStr << " AND i.id #{readCond} IN
                    (
                      SELECT item_id
                      FROM to_reads
                      WHERE user_id = #{userID}
                    )
      "
    end


    sqlStr  << " ORDER BY i.created_at"

    self.find_by_sql(sqlStr)
  end
  
  def self.getFeedItems(feedID)
    sqlStr ="
    SELECT distinct
      i.id
      ,i.title
      ,i.link
      ,i.abstract
      ,i.description
      ,i.created_at
      ,i.updated_at
      ,i.image_url
      ,i.feed_id
    FROM
      items i"
    unless feedID.nil?
      sqlStr << "  
    WHERE
      i.feed_id = #{feedID}"
    end
    self.find_by_sql(sqlStr)
  end

end

