class UserCategory < ActiveRecord::Base

  acts_as_list

  belongs_to :user
  has_many :feed_user_categories
  has_many :feeds, :through => :feed_user_categories

end
