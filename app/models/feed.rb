require 'uri'
require 'open-uri' #http request handler
require 'open_uri_redirections' #Fixes HTTP->HTTPS redirections
require 'nokogiri' #xml parser

class Feed < ActiveRecord::Base
  #include ActionView::Helpers::TextHelper # truncate
  #include ERB::Util # h

  #Relations
  belongs_to :provider
  belongs_to :category
  has_many :items
  has_many :feed_user_categories
  has_many :user_categories, :through => :feed_user_categories

  #Validations
  #  presence
  validates :url, :presence => true
  #  uniqueness
  validates :url, :uniqueness => true
  
  #Initializations
  before_save do |f|
    feedUrl = URI.encode(self.url)
    begin
      root = Nokogiri::XML(open(feedUrl))
    rescue
      #TODO: show error to user (probably site down or bad url)
      return false
    end
    root.remove_namespaces!
    
    root.xpath('feed').map do |feed|
      f.name = feed.xpath('title').text
    end
    root.xpath('//channel').map do |feed|
      f.name = feed.xpath('title').text || f.name
    end
    begin
      feedUrl = URI.encode(f.url)
      Nokogiri::XML(open(feedUrl))
    rescue Exception => e
      #flash.now[:error]="There was an error while checking the feed (#{e.message})"
      puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%" + e.message

      return false
    end
    return true
  end
  after_save :force_updateFeeds


  def updateFeeds
    force_updateFeeds if self.updated_at < (2.hours + 30.minutes).from_now
  end
  
  
  def force_updateFeeds
    feedUrl = URI.encode(self.url)
    root = Nokogiri::XML(open(feedUrl))
    root.remove_namespaces!
    
    if feedUrl =~ /atom/
      parseAtom root
    elsif feedUrl =~ /rss/
      parseRSS root
    else
      parseRSS root
      parseAtom root
    end
    
  end
  
  
  
  def parseRSS(root)
    puts "%%%%%%%%%%%%%%%%%%% Parsing RSS"
    #http://feed2.w3.org/docs/rss2.html
    
    #RSS 2.0 XML structure:
    #  rss
    #  channel
    #    title           The name of the channel. It's how people refer to your service
    #    link            The URL to the HTML website corresponding to the channel.
    #    description     Phrase or sentence describing the channel.
    #    item[]          //title or description must be present
    #      (title)         Venice Film Festival Tries to Quit Sinking
    #      (link)          http://www.nytimes.com/2002/09/07/movies/07fest.html
    #      (description)   Some of the most heated chatter at the Venice Film Festival this week was about the way that the arrival of the stars at the Palazzo del Cinema was being staged.
    #      (author)        oprah@oxygen.net
    #      (category)      Simpsons Characters
    #      (comments)      URL of a page for comments relating to the item
    #      (pubDate)       Sun, 19 May 2002 15:21:36 GMT
    
    #Update Feed
    self.updated_at = DateTime.now
    
    root.xpath('//channel').map do |feed|
      self.name = feed.xpath('title').text
    end
    
    #Update Items
    root.xpath('//channel/item').map do |entry|
      
      #parse
      title       = entry.xpath('title').text
      link        = entry.xpath('link').text
      description = entry.xpath('description').text
      
      #fix
      title       = nil if title.length<1
      link        = nil if link.length<1
      abstract    = nil
      description = nil if description.length<1
      
      begin
        Item.create(
          :feed_id     => self.id,
          :title       => title,
          :link        => link,
          :abstract    => abstract,
          :description => description
        )
      rescue Exception
        #TODO: display error to user
      end
    end
  end
  
  def parseAtom(root)
    puts "%%%%%%%%%%%%%%%%%%% Parsing Atom"
    #http://feed2.w3.org/docs/rss2.html
    
    #Atom 1.0 XML structure:
    #  feed
    #    title           .
    #    subtitle
    #    link            The URL to the HTML website corresponding to the channel.
    #    id
    #    entry[]
    #      id
    #      title           Venice Film Festival Tries to Quit Sinking
    #      link            http://www.nytimes.com/2002/09/07/movies/07fest.html
    #      description
    #      summary
    #      updated
    
    #Update Feed
    self.updated_at = DateTime.now
    root.xpath('feed').map do |feed|
      self.name = feed.xpath('title').text
    end
    
    root.xpath('feed/entry').map do |entry|
      title = entry.xpath('title').text
      linkNodes = entry.xpath('link')

      url = nil
      if linkNodes.length>1
        linkNodes.each do |linkNode|
          begin
            url = linkNode.attr('href').text if (linkNode.attr('type').text =~ /html/)
          rescue Exception
          end
        end
      end
      url = linkNodes.attr('href').text if url.nil? #default to first
      
      abstract    = parseFormmatedNode entry, 'abstract'
      summary     = parseFormmatedNode entry, 'summary'
      description = parseFormmatedNode entry, 'description'
      content     = parseFormmatedNode entry, 'content'
      
      itemId = entry.xpath('id').text
      if url=~URI::regexp
        link = url
      else
        link = itemId
      end
      link  = URI.join(self.url,url).to_s
      
      
      abstract = summary if !abstract || abstract.length<1
      abstract = nil     if abstract.length<1
      
      description = content  if !description || description.length<1
      description = nil      if description.length<1
      
      begin
        Item.create(
          :feed_id     => self.id,
          :title       => title,
          :link        => link,
          :abstract    => abstract,
          :description => description
        )
      rescue Exception
        #TODO: display error to user
      end
    end
  end


  ### parse helpers
  def tryGetText(node, xp)
    begin
      text=node.xpath(xp).text
    rescue Exception
      text=nil
    end
  end
  
  def parseFormmatedNode(node, xp)
    text = ''
    begin
      n = node.xpath(xp)
      text = n.text
      type = n.attr('type').text.downcase
      text = h(text) unless (type=~/html/)
    rescue Exception
    end
    text
  end
  
  def parseAttr(node, xp, a)
    attr = ''
    begin
      n = node.xpath(xp)
      attr = n.attr(a).text
    rescue Exception
    end
    attr
  end
  
  def parseTextNode(node,xp)
    t = ''
    begin
      n = node.xpath(xp)
      t = n.text
    rescue Exception
    end
    t
  end

end
