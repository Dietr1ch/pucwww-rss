class UsersController < ApplicationController



  def new
    @user = User.new
    @user.role = :role_user
  end
  
  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    render_forbidden and return unless can_edit?
    @user_categories = UserCategory.order("position").all
  end

  def create
    @user = User.new(user_params)

    if @user.save
      #redirect_to items_url, :notice => "Signed up!"
      session[:user_id] = @user.id
      redirect_to items_url, :notice => "Logged in!"
    else
      render "new"
    end
  end


  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    @user = User.find(params[:id])
    render_forbidden and return unless can_edit?

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to items_path, notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_password
    @user = User.find(params[:id])
  end

  def sort
    params[user_category_params].each_with_index do |id, index|
      UserCategory.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

  private
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  def user_category_params
    params.require(:user_category).permit(:name, :user_id, :position)
  end



end
