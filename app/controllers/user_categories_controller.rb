class UserCategoriesController < ApplicationController
  def new
    #TODO: restrict to owner and admins
    @user_category = UserCategory.new
  end

  def edit
    #TODO: restrict to owner and admins
    @user_category = UserCategory.find(params[:id])
  end

  def create
    #TODO: restrict to owner and admins
    #REVIEW: current_user can be nil
    @user_category = UserCategory.new(user_category_params)
    @user_category.user_id=current_user.id
    @user_category.position=@user_category.id

    if @user_category.save
      #REVIEW
      # send to new category???
      redirect_to items_path(:category_id => @user_category.id), :notice => "User Category created!"
    else
      #REVIEW: what should this do?
      render "new"
    end
  end

  def update
    #TODO: restrict to owner and admins
    @user_category = UserCategory.find(params[:id])

    respond_to do |format|
      if @user_category.update(user_category_params)
        format.html { redirect_to items_path, notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user_category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    #TODO: restrict to owner and admins
    @user_category = UserCategory.find(params[:id])
    @user_category.destroy
    redirect_to items_url
  end

  def sort
    params[user_category_params].each_with_index do |id, index|
      UserCategory.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end


  def user_category_params
    params.require(:user_category).permit(:name, :user_id, :position)
  end


end
