class ToReadsController < ApplicationController
  def create
    ToRead.create(:user_id=> current_user.id, :item_id => params[:item_id])
    redirect_to items_url
  end
end
