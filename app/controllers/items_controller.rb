class ItemsController < ApplicationController

  before_action :set_item, only: [:show, :read, :edit, :update, :destroy]

  # GET /items
  # GET /items.json
  def index

    unless current_user
      @title = "Latest News"
      @items = Item.getFeedItems(params[:feed_id])
      return
    end
    
    #current_user exists
    cat = params[:category_id]
    fID = params[:feed_id]
    
    #Menu
    @ucat  = user_categories.find_by_id(cat)
    #Content
    name = ""
    name = @ucat.name unless @ucat.nil?
    cFeed = Feed.find_by_id(fID)
    @title = "Latest #{name} News"
    
    unless cFeed.nil?
      unless cFeed.name.nil?
        if cFeed.name.length > 1
          @title << " from '#{cFeed.name}'"
        end
      end
    end
    
    @items = user_items

    respond_to do |format|
      format.html
      format.json { render json: @items}
    end

  end

  # GET /items/1
  # GET /items/1.json
  def show
    
    if current_user
      fueLeido = false
      UserReadItem.all.each do |uri|
        if uri.user_id == current_user.id && uri.item_id.to_s == params[:id]
          fueLeido = true
        end
      end
      if !fueLeido
        UserReadItem.create(:user_id=> current_user.id, :item_id => params[:id])
      end
  
      #leerDespues = false #unused..
      ToRead.all.each do |tr|
        if tr.user_id == current_user.id && tr.item_id.to_s == params[:id]
          ToRead.find(tr.id).destroy
        end
      end
    end

    respond_to do |format|
      format.html
      format.json { render json: @item}
    end
    
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  def read
    show
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      begin
        @item = Item.find(params[:id])
      rescue Exception
        redirect_to items_url, :notice => "Item not found :C, our monkeys are working on it!"
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:title, :link, :abstract, :description, :feed_id)
    end


end
