require 'rubygems'
require 'nokogiri'
require 'feedbag'

class FeedsController < ApplicationController

  def new
    @feed = Feed.new
    begin
      category_id = params[:category_id]
      @uCat = user_categories.find_by_id(category_id)
    rescue Exception
      #TODO: send to Latest News and inform missing cat error
    end
  end

  def edit
  end

  def show
    @feed = Feed.find(params[:id])
    respond_to do |format|
      format.json { render json: @feed }
    end
  end

  def create

    @feed = Feed.new(feed_params)

    @feed.category_id = params[:category_id]

    @uCat = user_categories.find_by_id(@feed.category_id)

    feedUrl = Feedbag.find(@feed.url).first()
    @feed.url = URI.encode(feedUrl)
    cat = params[:category_id]
    if @feed.save
        feedId = @feed.id
    else #not added
      @feed = Feed.find_all_by_url(@feed.url).first()
      if @feed #already exists
        feedId = @feed.id
      else #error ocurred :C
        #TODO: present an error to the user
        render "new"
      end
    end
    @feed_user_category = FeedUserCategory.create(:feed_id=> feedId, :user_category_id=>cat)
    redirect_to items_url(:category_id => cat), :notice => "Subscribed to '#{@feed.name}'"
  end

  def update
  end

  def destroy
  end

  def feed_params
    params.require(:feed).permit(:name, :url, :category_id)
  end


end
