class FeedUserCategoriesController < ApplicationController
  def destroy
    @feed_user_category = FeedUserCategory.find(params[:id])
    @feed_user_category.destroy
    redirect_to items_url
  end
end
