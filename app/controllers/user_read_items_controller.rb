class UserReadItemsController < ApplicationController
  def destroy
    @user_read_item = UserReadItem.find(params[:uri_id])
    @user_read_item.destroy
    redirect_to items_url
  end
end
