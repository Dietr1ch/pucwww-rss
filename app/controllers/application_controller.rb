class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


helper_method :current_user
helper_method :user_categories


private
  def current_user
    begin
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    rescue Exception
      @current_user = nil
    end
  end

  def can_edit?
    if current_user.nil?
      false
    else
      current_user.is_admin? || current_user == @user
    end
  end
  
  def user_categories
    UserCategory.where(:user_id => current_user)
  end


  
  #shortcut to getItems...
  def user_items
    Item.getItems(current_user.id, params[:category_id], params[:feed_id], params[:read], params[:later])
  end


  def render_forbidden
    redirect_to items_url, :notice => "Action Forbidden :C"
    true
  end


  def default_serializer_options
    {root: false}
  end


end
