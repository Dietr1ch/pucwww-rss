json.array!(@items) do |item|
  json.extract! item, :title, :link, :abstract, :description
  json.url item_url(item, format: :json)
end