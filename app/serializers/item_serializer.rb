class ItemSerializer < ActiveModel::Serializer
  attributes :id,
    :title,
    :link,
    :abstract,
    :description,
    :created_at,
    :updated_at,
    :image_url,
    :feed_id,
    :item_link

  def item_link
    item_url(object)
  end
end
