class FeedSerializer < ActiveModel::Serializer
  attributes :id,
    :name,
    :url,
    :created_at,
    :updated_at,
    :provider_id,
    :category_id

  has_many :items
end
