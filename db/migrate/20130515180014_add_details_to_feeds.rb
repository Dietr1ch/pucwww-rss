class AddDetailsToFeeds < ActiveRecord::Migration
  def change
    add_column :feeds, :provider_id, :integer
    add_column :feeds, :category_id, :integer
  end
end
