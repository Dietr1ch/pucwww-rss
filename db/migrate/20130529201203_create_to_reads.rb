class CreateToReads < ActiveRecord::Migration
  def change
    create_table :to_reads do |t|
      t.integer :user_id
      t.integer :item_id

      t.timestamps
    end
  end
end
