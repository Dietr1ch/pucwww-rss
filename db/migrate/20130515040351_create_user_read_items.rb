class CreateUserReadItems < ActiveRecord::Migration
  def change
    create_table :user_read_items do |t|

      t.timestamps
    end
  end
end
