class AddDetailsToUserReadItems < ActiveRecord::Migration
  def change
    add_column :user_read_items, :user_id, :integer
    add_column :user_read_items, :item_id, :integer
  end
end
