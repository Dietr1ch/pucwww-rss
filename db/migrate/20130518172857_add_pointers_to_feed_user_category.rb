class AddPointersToFeedUserCategory < ActiveRecord::Migration
  def change
    add_column :feed_user_categories, :feed_id, :integer
    add_column :feed_user_categories, :user_category_id, :integer
  end
end
