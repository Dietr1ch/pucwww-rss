class AddUserIdToUserCategory < ActiveRecord::Migration
  def change
    add_column :user_categories, :user_id, :integer
  end
end
