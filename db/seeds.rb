# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Item.delete_all
Item.create(
  :title       => 'Codelco invertira US$ 80 millones en planta de recuperacion de metales en Mejillones',
  :link        => 'http://www.emol.com/noticias/economia/2013/04/24/595221/codelco-invertira-us-80-millones-en-planta-de-recuperacion-de-metales-en-mejillones.html',
  :abstract    => 'abstract codelco',
  :description => 'La minera estatal ingreso a evaluacion ambiental el proyecto y se espera que inicie su construccion en enero de 2014.'
)

Item.create(
  :title       => 'Goleador Sebastian Pol anuncio que no seguira en Cobreloa tras el presente torneo',
  :link        => 'http://www.emol.com/noticias/deportes/2013/04/24/595219/goleador-sebastian-pol-anuncio-que-no-seguira-en-cobreloa-tras-el-presente-torneo.html',
  :abstract    => 'abstract cobreloa',
  :description => 'El argentino dice que tomo la decision luego del poco interes mostrado por los dirigentes en su renovacion.',
  :image_url   => 'http://img.emol.com/2013/04/24/21441871---universidad-de-chile-vs-cobreloa---30032013---235603_134131.jpg'
)

Item.create(
  :title       => 'Chile fue la sede con mas participantes en la hackathon "Space Apps Challenge" de la NASA',
  :link        => 'http://www.emol.com/noticias/tecnologia/2013/04/24/595148/chile-fue-la-sede-con-mas-participantes-en-la-hackathon-space-apps-challenge-de-la-nasa.html',
  :abstract    => 'abstract space apps challenge',
  :description => '34 equipos participaron en el encuentro realizado el fin de semana pasado, donde se intento solucionar desafios de la NASA y del observatorio ALMA.',
  :image_url   => 'http://img.emol.com/2013/04/24/spaceapps1979_92822.jpg'
)
